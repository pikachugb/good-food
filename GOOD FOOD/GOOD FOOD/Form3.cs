﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace GOOD_FOOD
{
    public partial class Form3 : Form
    {
        SqlConnection cn;
        SqlCommand cmd = new SqlCommand();
        SqlDataReader dr;
        public Form3()
        {
            InitializeComponent();
            textBox2.PasswordChar = '*';
            string cale = Application.StartupPath;
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename="+cale+@"\GOOD_FOOD.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
        }

        public int global { get; set; }

        void sf4()
        {
            Application.Run(new Form4());
        }
        
        void sf1()
        {
            Application.Run(new Form1());
        }

        public static class ints
        {
            public static int globals { get; set; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cn.Open();
            cmd.CommandText = "Select id_client from Clienti where email='" + textBox1.Text + "' and parola='" + textBox2.Text + "'";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                {
                    
                    ints.globals = int.Parse(dr[0].ToString());
                    Thread t = new Thread(new ThreadStart(sf4));
                    t.Start();
                    this.Close();
                }
            else
                MessageBox.Show("Eroare autentificare!");
            cn.Close();
        }

        private void revenireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(new ThreadStart(sf1));
            t.Start();
            this.Close();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1.PerformClick();
        }
    }
}
