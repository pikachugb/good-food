﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.IO;

namespace GOOD_FOOD
{
    public partial class Form4 : Form
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn;
        SqlDataReader dr;
        Button[] adauga = new Button[1000];
        string cale;
        Graphics g;
        Pen p = new Pen(Color.Black);
        SolidBrush b = new SolidBrush(Color.DarkCyan);
        SolidBrush n = new SolidBrush(Color.Black);
        string[] men = new string[15];
        int idclient,nrmen=0,totalcal=0,totalpret=0,t=0,idcomanda=0,pcom=0;
        Boolean first = false;
        Form3 f3 = new Form3();
        Boolean paint = false;
        public Form4()
        {
            InitializeComponent();
            cale = Application.StartupPath;
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=" + cale + @"\GOOD_FOOD.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
            idclient = Form3.ints.globals;
        }
        
        private void Form4_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'gOOD_FOODDataSet1.Meniu' table. You can move, or remove it, as needed.
            this.meniuTableAdapter.Fill(this.gOOD_FOODDataSet1.Meniu);
        }

        private void iesireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private Boolean verificari()
        {
            Boolean intr;
            int t1;
            intr = int.TryParse(textBox1.Text,out t1);
            if (intr == false)
                return false;
            intr = int.TryParse(textBox2.Text, out t1);
            if (intr == false)
                return false;
            intr = int.TryParse(textBox3.Text, out t1);
            if (intr == false)
                return false;
            return true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox4.Clear();
            if (verificari())
            {
                int s = int.Parse(textBox1.Text) + int.Parse(textBox2.Text) + int.Parse(textBox3.Text);
                if (s < 250)
                    textBox4.Text = "1800";
                else
                    if (s >= 250 && s <= 275)
                        textBox4.Text = "2200";
                    else
                        if (s > 275)
                            textBox4.Text = "2500";
                cn.Open();
                cmd.CommandText = "Update Clienti set kcal_zilnice='" + int.Parse(textBox4.Text) + "'";
                cmd.ExecuteNonQuery();
                cn.Close();
                MessageBox.Show("Kcal au fost trecute in baza de date!");
            }
            else
                MessageBox.Show("Introduceti doar numere!");
        }

        private void incarcadatemeniu()
        {
            nrmen = 0;
            cn.Open();
            cmd.CommandText = "Delete from Meniu";
            cmd.ExecuteNonQuery();
            cn.Close();

            string txt = cale;
            txt=txt.Replace(@"\", "/");
            txt=txt.Substring(0, txt.Length-2*Application.ProductName.Length-2);
            StreamReader sr = new StreamReader(txt+"/meniu.txt");
            string line;
            sr.ReadLine();
            while ((line = sr.ReadLine()) != "")
            {
                men = line.Split(';');
                cn.Open();
                cmd.CommandText = "Insert into Meniu(id_produs,denumire_produs,descriere,pret,kcal,felul) values ('" + int.Parse(men[0]) + "','" + men[1] + "','" + men[2] + "','" + int.Parse(men[3]) + "','" + int.Parse(men[4]) + "','" + int.Parse(men[5]) + "')";
                cmd.ExecuteNonQuery();
                cn.Close(); 
                nrmen++;
            }
        }

        private void introducere_comanda()
        {
            cn.Open();
            cmd.CommandText = "Select id_comanda from Comenzi";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while (dr.Read())
                    idcomanda = int.Parse(dr[0].ToString());
            else
                idcomanda = 0;
            idcomanda++;
            cn.Close();
            cn.Open();
            cmd.CommandText = "Insert into Comenzi(id_comanda,id_client,data_comanda) values('"+idcomanda.ToString()+"','"+idclient+"','"+DateTime.Now+"')";
            cmd.ExecuteNonQuery();
            cn.Close();
        }


        private void tabPage2_Enter(object sender, EventArgs e)
        {
            if (first == false)
            {
                if (textBox4.Text != "")
                {
                    textBox5.Text = textBox4.Text;
                    incarcadatemeniu();
                    introducere_comanda();
                    first = true;
                }
                else
                {
                    MessageBox.Show("Faceti calcul pentru Kcal-iile zilnice!!!!!");
                    tabControl1.SelectedTab = tabPage1;
                }
            }
        }

        public string[] com = new string[100];
        public int[] comcal = new int[100];
        public int[] compret = new int[100];
        public int[] comcan = new int[100];
        public int[] comidprod = new int[100];

        private void introducere_subcomanda()
        {
            for (int i = 0; i < t; i++)
            {
                cn.Open();
                cmd.CommandText = "Insert into Subcomenzi(id_comanda,id_produs,cantitate) values ('"+idcomanda.ToString()+"','"+comidprod[i]+"','"+comcan[i]+"')";
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedCells[0].ColumnIndex == 7)
            {
                int apasat = 0;
                apasat = dataGridView1.SelectedCells[0].RowIndex ;
                int cantitate = int.Parse(dataGridView1[6, apasat].FormattedValue.ToString());
                int cal = int.Parse(dataGridView1[4, apasat].FormattedValue.ToString());
                int pret = int.Parse(dataGridView1[3, apasat].FormattedValue.ToString());
                string denumire = dataGridView1[1, apasat].FormattedValue.ToString();
                int idprodus = int.Parse(dataGridView1[0, apasat].FormattedValue.ToString());
                if (cantitate >= 1)
                {
                    totalcal = totalcal + cantitate*cal;
                    totalpret = totalpret + cantitate * pret;
                    textBox6.Text = totalcal.ToString();
                    textBox7.Text = totalpret.ToString();
                    com[t] = denumire;
                    compret[t] = pret;
                    comcal[t] = cal;
                    comcan[t] = cantitate;
                    comidprod[t] = idprodus;
                    t++;
                    pcom++;
                }
                else
                    MessageBox.Show("Cantitate negativa");
            }
        }

        private void tabPage3_Enter(object sender, EventArgs e)
        {
            
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView2.SelectedCells[0].ColumnIndex == 7)
            {
                int apasat;
                apasat = dataGridView1.SelectedCells[0].RowIndex;
                cn.Open();
                cmd.CommandText = "Delete from Subcomenzi where id_comanda='" + idcomanda + "' and id_produs='" + comidprod[apasat-1] + "'";
                cmd.ExecuteNonQuery();
                cn.Close();
                dataGridView2.Rows.Remove(dataGridView2.Rows[apasat-1]);
                int tk = int.Parse(textBox9.Text);
                int tp = int.Parse(textBox10.Text);
                tk = tk - comcal[apasat - 1];
                tp = tp - compret[apasat - 1];
                textBox10.Text = tp.ToString();
                textBox9.Text = tk.ToString();
                pcom--;
            }
        }

        void sf1()
        {
            Form1 f1 = new Form1();
            Application.Run(f1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void axe()
        {
            int w=panel1.Width;
            int h = panel1.Height;

            g.DrawLine(p, 20, h - 20, w - 20, h - 20);
            g.DrawLine(p, 20, h - 92, w - 20, h - 92);
            g.DrawLine(p, 20, h - 164, w - 20, h - 164);
            g.DrawLine(p, 20, h - 236, w - 20, h - 236);
            g.DrawLine(p, 20, h - 308, w - 20, h - 308);
            g.DrawLine(p, 20, h - 380, w - 20, h - 380);

            g.DrawLine(p, w-20, 20, w-20, h - 20);
            g.DrawLine(p, 20, 20, 20, h - 20);

            g.DrawString("0", Font, n, 10, h - 30);
            g.DrawString("200", Font, n, 0, h - 102);
            g.DrawString("400", Font, n, 0, h - 174);
            g.DrawString("600", Font, n, 0, h - 246);
            g.DrawString("800", Font, n,0, h - 318);
            g.DrawString("1000", Font, n, 0, h - 390);
        }

        private void fills()
        {
            int h = panel1.Height;
            int loc = 30;
            for (int i = 0; i < pcom; i++)
            {
                int inal = comcan[i] * comcal[i];
                inal = inal * 72 / 200;
                g.FillRectangle(b, loc, h - 20-inal, 20, inal);
                loc += 30;
            }
        }

        private void tabPage4_Enter(object sender, EventArgs e)
        {
            paint = true ;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (paint)
            {
                g = panel1.CreateGraphics();
                g.Clear(Color.White);
                axe();
                fills();
            }
        }

        private void tabPage4_Leave(object sender, EventArgs e)
        {
            paint = false;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Comanda trimisa!");
            Thread t = new Thread(new ThreadStart(sf1));
            t.Start();
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            button4.Visible = false;
            introducere_subcomanda();
            for (int i = 0; i < t; i++)
            {
                dataGridView2.Rows.Add(com[i],comcal[i],compret[i],comcan[i]);
            }
        }

    }
}
