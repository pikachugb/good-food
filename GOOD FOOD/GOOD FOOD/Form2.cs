﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace GOOD_FOOD
{
    public partial class Form2 : Form
    {
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn;
        SqlDataReader dr;
        Boolean exista = false;
        public Form2()
        {
            InitializeComponent();
            string cale = Application.StartupPath;
            cale = cale.Substring(0, cale.Length - 10);
            cn = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename="+cale+@"\GOOD_FOOD.mdf;Integrated Security=True;User Instance=True");
            cmd.Connection = cn;
            textBox4.PasswordChar = '*';
            textBox5.PasswordChar = '*';
        }

        private void verificari()
        {
            cn.Open();
            cmd.CommandText = "Select nume,prenume,adresa from Clienti";
            dr = cmd.ExecuteReader();
            if (dr.HasRows)
                while(dr.Read())
                    if (dr[0].ToString()==textBox1.Text&&dr[1].ToString()==textBox2.Text&&dr[2].ToString()==textBox3.Text)
                        exista=true;
            cn.Close();

            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" && textBox6.Text != "")
            {
                if (textBox4.Text == textBox5.Text)
                {
                    if (exista==false)
                    {
                        cn.Open();
                        cmd.CommandText = "Insert into Clienti(nume,prenume,adresa,parola,email,kcal_zilnice) values('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + textBox6.Text + "','"+2000+"')";
                        cmd.ExecuteNonQuery();
                        cn.Close();
                        MessageBox.Show("Ati fost adaugat!");
                        this.Close();
                    }
                    else
                        MessageBox.Show("Persoana exista in baza noastra de date!");
                }
                else
                    MessageBox.Show("Parolele nu corespund!");
            }
            else
                MessageBox.Show("Toate campuriile sunt obligatorii!");
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
            verificari();
        }

        private void revenireToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

